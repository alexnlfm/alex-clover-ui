# alex-clover-ui

> UI components for React app with Clover themimg

[![NPM](https://img.shields.io/npm/v/alex-clover-ui.svg)](https://www.npmjs.com/package/alex-clover-ui) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save alex-clover-ui
```

## Description

This package provides following components: HeaderLogo, LinkButton, TextInput and SpaceMakerBox

Below are the props list that each components accepts:
```
HeaderLogo: brandName, icon, iconAriaLabel, fontSize, spaceBetweenIconAndText
```

```
LinkButton: linkUrl, buttonText, fontSize, padding
```

```
TextInput: shouldBeFocused, labelText, type, id, placeholder, value, onChangeHandler, fontSize
```

```
SpaceMakerBox: p (padding), px (right and left padding), py (top and bottom padding), pt (top padding), pr (right padding), pb (bottom padding) pl (left padding), m (margin), mx (right and left margin), my (top and bottom margin), mt (top margin), mr (right margin), mb (bottom margin), ml (left margin)
```

## License

MIT © [alexnlfm](https://github.com/alexnlfm)
