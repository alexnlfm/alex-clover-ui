import React from 'react';
import { Router } from '@reach/router';
import renderer from 'react-test-renderer';
import { render } from '@testing-library/react';

import AlexCloverHeaderLogo from './AlexCloverHeaderLogo';

describe('AlexCloverHeaderLogo', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <Router>
          <AlexCloverHeaderLogo
            brandName="test"
            icon="icon placeholder"
            iconAriaLabel="test aria label"
            path="/"
          />
        </Router>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('displays brand name with all uppercase latters', () => {
    const { getByText } = render(
      <Router>
        <AlexCloverHeaderLogo
          brandName="test"
          icon="icon placeholder"
          iconAriaLabel="test aria label"
        />
      </Router>
    );
    const element = getByText('TEST');
    expect(element).toBeTruthy();
  });
});
