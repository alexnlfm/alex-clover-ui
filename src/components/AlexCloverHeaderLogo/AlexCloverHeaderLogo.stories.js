import React from 'react';
import AlexCloverHeaderLogo from './AlexCloverHeaderLogo';

export default {
  component: AlexCloverHeaderLogo,
  title: 'Header Logo'
};

export const defaultExample = () => (
  <AlexCloverHeaderLogo
    brandName="Some fancy name"
    icon="👍"
    iconAriaLabel="thumbs up icon"
    spaceBetweenIconAndText="5px"
  />
);

export const widerSpaceBetweenIconAndText = () => (
  <AlexCloverHeaderLogo
    brandName="Some fancy name"
    icon="👍"
    iconAriaLabel="thumbs up icon"
    spaceBetweenIconAndText="20px"
  />
);

export const bigFontSize = () => (
  <AlexCloverHeaderLogo
    brandName="Some fancy name"
    icon="👍"
    iconAriaLabel="thumbs up icon"
    spaceBetweenIconAndText="5px"
    fontSize="3rem"
  />
);
