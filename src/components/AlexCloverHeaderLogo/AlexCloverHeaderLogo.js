import React from 'react';
import PropTypes from 'prop-types';
import styled, { ThemeProvider } from 'styled-components';

import theme from '../../styles/theme';

export default function AlexCloverHeaderLogo({
  brandName,
  icon,
  iconAriaLabel,
  fontSize,
  spaceBetweenIconAndText
}) {
  return (
    <ThemeProvider theme={theme}>
      <StyledDiv fontSize={fontSize}>
        <StyledSpan
          role="img"
          aria-label={iconAriaLabel}
          spaceBetweenIconAndText={spaceBetweenIconAndText}
        >
          {icon}
        </StyledSpan>
        <span>{brandName.toUpperCase()}</span>
      </StyledDiv>
    </ThemeProvider>
  );
}
AlexCloverHeaderLogo.propTypes = {
  brandName: PropTypes.string.isRequired,
  icon: PropTypes.node.isRequired,
  iconAriaLabel: PropTypes.string.isRequired,
  fontSize: PropTypes.string,
  spaceBetweenIconAndText: PropTypes.string
};

const StyledDiv = styled.div`
  display: flex;
  align-items: center;
  color: ${props => props.theme.palette.primaryTextColor};
  font-size: ${props => props.fontSize};
`;

const StyledSpan = styled.span`
  margin-right: ${props => props.spaceBetweenIconAndText};
`;
