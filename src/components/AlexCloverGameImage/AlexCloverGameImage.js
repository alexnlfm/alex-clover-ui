import React from 'react';
import PropTypes from 'prop-types';
import styled, { ThemeProvider } from 'styled-components';

import theme from '../../styles/theme';

export default function AlexCloverGameImage({
  src,
  alt,
  size,
  objectPosition,
  mixBlendMode
}) {
  return (
    <ThemeProvider theme={theme}>
      <StyledImg
        src={src}
        alt={alt}
        size={size}
        objectPosition={objectPosition}
        mixBlendMode={mixBlendMode}
      />
    </ThemeProvider>
  );
}
AlexCloverGameImage.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  size: PropTypes.string.isRequired,
  objectPosition: PropTypes.string,
  mixBlendMode: PropTypes.string
};

const StyledImg = styled.img`
  width: ${props => props.size};
  min-width: ${props => props.size};
  height: ${props => props.size};
  object-fit: cover;
  object-position: ${props => props.objectPosition || 'initial'};
  border: 5px solid ${props => props.theme.palette.primaryColor};
  border-radius: 100%;
  mix-blend-mode: ${props => props.mixBlendMode || 'unset'};
`;
