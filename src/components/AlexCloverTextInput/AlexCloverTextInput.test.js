import React from 'react';
import renderer from 'react-test-renderer';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';

import AlexCloverTextInput from './AlexCloverTextInput';

describe('AlexCloverTextInput', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <AlexCloverTextInput
          labelText="Test"
          type="text"
          id="test"
          onChange={() => 1 === 1}
          value="test"
          fontSize="10px"
        />
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('gets focused when shouldBeFocused props is true', () => {
    const { getByDisplayValue } = render(
      <AlexCloverTextInput
        shouldBeFocused={true}
        labelText="Test"
        type="text"
        id="test"
        onChange={() => 1 === 1}
        value="something"
        fontSize="10px"
      />
    );
    const input = getByDisplayValue('something');
    expect(input).toHaveFocus();
  });

  it("doesn't get focused when shouldBeFocused props is false", () => {
    const { getByDisplayValue } = render(
      <AlexCloverTextInput
        shouldBeFocused={false}
        labelText="Test"
        type="text"
        id="test"
        onChange={() => 1 === 1}
        value="something"
        fontSize="10px"
      />
    );
    const input = getByDisplayValue('something');
    expect(input).not.toHaveFocus();
  });
});
