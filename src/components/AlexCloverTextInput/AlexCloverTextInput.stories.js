import React, { useState } from 'react';
import AlexCloverTextInput from './AlexCloverTextInput';

export default {
  component: AlexCloverTextInput,
  title: 'Text Input'
};

export const defaultExample = () => {
  const [inputValue, setInputValue] = useState();

  const handleInputFieldChange = e => {
    setInputValue(e.target.value);
  };

  return (
    <AlexCloverTextInput
      labelText="Text field"
      type="text"
      id="unique-id"
      value={inputValue}
      onChange={handleInputFieldChange}
      fontSize="1rem"
    />
  );
};

export const withInitialValue = () => {
  const [inputValue, setInputValue] = useState('initial value');

  const handleInputFieldChange = e => {
    setInputValue(e.target.value);
  };

  return (
    <AlexCloverTextInput
      labelText="Text field"
      type="text"
      id="unique-id"
      value={inputValue}
      onChange={handleInputFieldChange}
      fontSize="1rem"
    />
  );
};

export const withPlaceholderText = () => {
  const [inputValue, setInputValue] = useState();

  const handleInputFieldChange = e => {
    setInputValue(e.target.value);
  };

  return (
    <AlexCloverTextInput
      labelText="Text field"
      type="text"
      id="unique-id"
      placeholder="this is a placeholder text..."
      value={inputValue}
      onChange={handleInputFieldChange}
      fontSize="1rem"
    />
  );
};

export const focused = () => {
  const [inputValue, setInputValue] = useState();

  const handleInputFieldChange = e => {
    setInputValue(e.target.value);
  };

  return (
    <AlexCloverTextInput
      shouldBeFocused={true}
      labelText="Text field"
      type="text"
      id="unique-id"
      placeholder="this is a placeholder text..."
      value={inputValue}
      onChange={handleInputFieldChange}
      fontSize="1rem"
    />
  );
};

export const biggerFontSize = () => {
  const [inputValue, setInputValue] = useState('initial value');

  const handleInputFieldChange = e => {
    setInputValue(e.target.value);
  };

  return (
    <AlexCloverTextInput
      labelText="Text field"
      type="text"
      id="unique-id"
      value={inputValue}
      onChange={handleInputFieldChange}
      fontSize="2rem"
    />
  );
};
