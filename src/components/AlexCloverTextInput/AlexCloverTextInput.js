import React from 'react';
import PropTypes from 'prop-types';
import styled, { ThemeProvider } from 'styled-components';

import theme from '../../styles/theme';

export default class AlexCloverTextInput extends React.Component {
  inputRef = React.createRef();
  componentDidMount() {
    if (this.props.shouldBeFocused) {
      this.inputRef.current.focus();
    }
  }

  render() {
    const {
      labelText,
      type,
      id,
      placeholder,
      value,
      onChange,
      fontSize
    } = this.props;
    return (
      <ThemeProvider theme={theme}>
        <StyledLabel forHtml={id} fontSize={fontSize}>
          {labelText}
          <StyledInput
            ref={this.inputRef}
            type={type}
            id={id}
            placeholder={placeholder}
            value={value}
            onChange={onChange}
            fontSize={fontSize}
          />
        </StyledLabel>
      </ThemeProvider>
    );
  }
}
AlexCloverTextInput.propTypes = {
  shouldBeFocused: PropTypes.bool,
  labelText: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  fontSize: PropTypes.string.isRequired
};

const StyledLabel = styled.label`
  display: flex;
  flex-direction: column;
  font-size: ${props => props.fontSize};
  color: ${props => props.theme.palette.secondaryTextColor};
`;

const StyledInput = styled.input`
  font-size: ${props => props.fontSize};
`;
