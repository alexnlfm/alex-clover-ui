import React from 'react';
import renderer from 'react-test-renderer';

import AlexCloverSpaceMakerBox from './AlexCloverSpaceMakerBox';

describe('AlexCloverSpaceMakerBox', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <AlexCloverSpaceMakerBox>
          <div>Test</div>
        </AlexCloverSpaceMakerBox>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
    expect(tree.children[0].type).toBe('div');
    expect(tree.children[0].children[0]).toBe('Test');
  });
});
