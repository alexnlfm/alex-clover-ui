import React from 'react';
import styled from 'styled-components';
import AlexCloverSpaceMakerBox from './AlexCloverSpaceMakerBox';

export default {
  component: AlexCloverSpaceMakerBox,
  title: 'Space Maker Box'
};

export const addingPadding = () => (
  <StyledDiv>
    <AlexCloverSpaceMakerBox p="20px">
      <span>Placeholder text</span>
    </AlexCloverSpaceMakerBox>
  </StyledDiv>
);

export const addingMargin = () => (
  <AlexCloverSpaceMakerBox m="20px">
    <StyledDiv>
      <span>Placeholder text</span>
    </StyledDiv>
  </AlexCloverSpaceMakerBox>
);

const StyledDiv = styled.div`
  border: 1px solid black;
`;
