import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

export default function AlexCloverSpaceMakerBox({
  children,
  p,
  px,
  py,
  pt,
  pr,
  pb,
  pl,
  m,
  mx,
  my,
  mt,
  mr,
  mb,
  ml
}) {
  return (
    <StyledDiv
      p={p}
      px={px}
      py={py}
      pt={pt}
      pr={pr}
      pb={pb}
      pl={pl}
      m={m}
      mx={mx}
      my={my}
      mt={mt}
      mr={mr}
      mb={mb}
      ml={ml}
    >
      {children}
    </StyledDiv>
  );
}
AlexCloverSpaceMakerBox.propTypes = {
  p: PropTypes.string,
  px: PropTypes.string,
  py: PropTypes.string,
  pt: PropTypes.string,
  pr: PropTypes.string,
  pb: PropTypes.string,
  pl: PropTypes.string,

  m: PropTypes.string,
  mx: PropTypes.string,
  my: PropTypes.string,
  mt: PropTypes.string,
  mr: PropTypes.string,
  mb: PropTypes.string,
  ml: PropTypes.string
};

const StyledDiv = styled.div`
  box-sizing: border-box;

  padding-top: ${props => props.p || props.py || props.pt};
  padding-right: ${props => props.p || props.px || props.pr};
  padding-bottom: ${props => props.p || props.py || props.pb};
  padding-left: ${props => props.p || props.px || props.pl};

  margin-top: ${props => props.m || props.my || props.mt};
  margin-right: ${props => props.m || props.mx || props.mr};
  margin-bottom: ${props => props.m || props.my || props.mb};
  margin-left: ${props => props.m || props.mx || props.ml};
`;
