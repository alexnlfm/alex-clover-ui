import React from 'react';
import PropTypes from 'prop-types';
import styled, { ThemeProvider } from 'styled-components';

import theme from '../../styles/theme';

export default function AlexCloverRoundedButton({
  buttonText,
  onClick,
  width,
  height,
  fontSize
}) {
  return (
    <ThemeProvider theme={theme}>
      <StyledButton
        onClick={onClick}
        width={width}
        height={height}
        fontSize={fontSize}
      >
        {buttonText}
      </StyledButton>
    </ThemeProvider>
  );
}
AlexCloverRoundedButton.propTypes = {
  buttonText: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  width: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  fontSize: PropTypes.string
};

const StyledButton = styled.button`
  width: ${props => props.width};
  height: ${props => props.height};
  font-size: ${props => props.fontSize};
  font-weight: bold;
  border-radius: 100%;
  cursor: pointer;
  border: none;
  background-color: ${props => props.theme.palette.primaryColor};
  color: ${props => props.theme.palette.primaryTextColor};
`;
