import React from 'react';
import AlexCloverButton from './AlexCloverButton';

export default {
  component: AlexCloverButton,
  title: 'Button'
};

export const defaultExample = () => (
  <AlexCloverButton buttonText="Click me!" onClick={handleButtonClick} />
);

export const bigFont = () => (
  <AlexCloverButton
    buttonText="I'm BIG!"
    onClick={handleButtonClick}
    fontSize="3rem"
  />
);

export const padding = () => (
  <AlexCloverButton
    buttonText="Button with padding"
    onClick={handleButtonClick}
    padding="20px"
  />
);

function handleButtonClick() {
  alert('Button was clicked');
}
