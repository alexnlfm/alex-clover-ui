import React from 'react';
import PropTypes from 'prop-types';
import styled, { ThemeProvider } from 'styled-components';

import theme from '../../styles/theme';

export default function AlexCloverButton({
  buttonText,
  onClick,
  width,
  minWidth,
  fontSize,
  padding
}) {
  return (
    <ThemeProvider theme={theme}>
      <StyledButton
        onClick={onClick}
        width={width}
        minWidth={minWidth}
        fontSize={fontSize}
        padding={padding}
      >
        {buttonText}
      </StyledButton>
    </ThemeProvider>
  );
}
AlexCloverButton.propTypes = {
  buttonText: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  fontSize: PropTypes.string,
  padding: PropTypes.string
};

const StyledButton = styled.button`
  width: ${props => props.width || ''};
  min-width: ${props => props.minWidth || ''};
  cursor: pointer;
  border: none;
  border-radius: 10px;
  font-size: ${props => props.fontSize};
  font-weight: bold;
  padding: ${props => props.padding};
  background-color: ${props => props.theme.palette.primaryColor};
  color: ${props => props.theme.palette.primaryTextColor};

  @media screen and (max-width: 1400px) {
    transform: scale(0.8);
  }

  @media screen and (max-width: 1024px) {
    transform: scale(0.6);
  }
`;
