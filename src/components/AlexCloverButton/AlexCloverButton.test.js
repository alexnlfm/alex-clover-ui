import React from 'react';
import renderer from 'react-test-renderer';

import AlexCloverButton from './AlexCloverButton';

describe('AlexCloverButton', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <AlexCloverButton
          buttonText="Click me"
          onClick={() => console.log('Button was clicked')}
        />
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
