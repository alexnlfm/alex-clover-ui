const primaryColor = '#2db14b';
const primaryTextColor = '#f6e9e9';
// const secondaryColor = '#272121';
const secondaryTextColor = '#d8dede';
// const inverse = '#363333';
// const positive = '#008c5a';
// const negative = '#ca0303';
// const dark = '#363333';

const theme = {
  palette: {
    primaryColor,
    primaryTextColor,
    secondaryTextColor
  }
};

export default theme;
